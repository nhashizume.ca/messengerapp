Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  get '/messages' => 'messages#index'
  get '/messages/new' => 'messages#new'

  post 'messages' => 'messages#create'
  # Defines the root path route ("/")
  # root "articles#index"
end
